package main
import (
	// "log"
	"testing"
	driver "github.com/neo4j/neo4j-go-driver/neo4j"
)

func populateDb(fn func(driver.Transaction)) {
 	db := opendb()
  defer db.Close()
  session := createSession(db)
  defer session.Close()
  tx, err := session.BeginTransaction()
  if err != nil {
  	panic(err)

  }
  _, err = tx.Run(`
  	MERGE (te1:TELEGRAM_USER {id: 1})
  	MERGE (te2:TELEGRAM_USER {id: 2})
  	MERGE (mu1:MONO_USER {id: "1"})
  	MERGE (mu2:MONO_USER {id: "2"})
  	MERGE (te1)<-[b1:BELONGS {nickname: "te1"}]-(mu1)
  	MERGE (te2)<-[b2:BELONGS {nickname: "te2"}]-(mu2)
  	MERGE (st1:STATEMENT {id: "1"})
  	MERGE (mu1)<-[:BELONGS]-(st1)
  	MERGE (st2:STATEMENT {id: "2"})
  	MERGE (mu2)<-[:BELONGS]-(st2)
  	`, nil)
  if err != nil {
      panic(err)
  }

  // defer tx.Commit()
  defer tx.Close()
  fn(tx)
  
}

func Test1(t *testing.T){
	populateDb(func(tx driver.Transaction){
		if list, _ := getTelegramUsersByTokenTx(tx, "1"); len(list) != 1 {
			t.Error("list len", len(list))
		}
		if err := delOrphanMonoUsersTx(tx); err != nil {
			t.Error(err)
		}
		if list, _ := getTelegramUsersByTokenTx(tx, "1"); len(list) != 1 {
			t.Error("list len2", len(list))
		}
	})
}
	
func Test2(t *testing.T){
	populateDb(func(tx driver.Transaction){
		res, err := tx.Run("MATCH (s:STATEMENT) return count(s)", nil)
		if err != nil {
			t.Error(err)
		}
		var statment_count int64
		for res.Next() {
			statment_count = res.Record().GetByIndex(0).(int64)
		}

		if nickname, _ := delTokenTx(tx, 1, "1"); nickname != "te1" {
			t.Error("expected te1 but got ", nickname)
		}
		if err := delOrphanMonoUsersTx(tx); err != nil {
			t.Error(err)
		}
		if list, _ := getTelegramUsersByTokenTx(tx, "1"); len(list) != 0 {
			t.Error("list len", len(list))
		}
		if list, _ := getTelegramUsersByTokenTx(tx, "2"); len(list) != 1 {
			t.Error("list len2", len(list))
		}
		res, err = tx.Run("MATCH (s:STATEMENT) return count(s)", nil)
		if err != nil {
			t.Error(err)
		}
		var c int64
		for res.Next() {
			c = res.Record().GetByIndex(0).(int64)
		}
		if statment_count - c != 1 {
			t.Error("c=",c)
		}

	})

}

func TestgetCurrencyStr(t *testing.T) {
		populateDb(func(tx driver.Transaction){

		if curr, _ := getCurrencyStr(985); curr != "PLN" {
			t.Error("not PLN, but ", curr)
		}
	})
}
func TestAddError(t *testing.T) {
	populateDb(func(tx driver.Transaction){
		c, err := getCountTx(tx, "match (e:ERROR) return count(e)")
		if err != nil {
			t.Error(err)
		}

		err = addErrorTx(tx, "test1", "test2")
		if err != nil {
			t.Error(err)
		}
		c2, err := getCountTx(tx, "match (e:ERROR) return count(e)")
		if err != nil {
			t.Error(err)
		}
		if c2 - c != 1 {
			t.Errorf("expected %d, got %d",c+1, c2)
		}

	})
}
