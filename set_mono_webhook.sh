#!/bin/sh
curl -v -X POST \
  https://api.monobank.ua/personal/webhook \
  -H 'content-type: application/json' \
  -H 'X-Token: '"$MY_MONO"'' \
  -d '{ "webHookUrl": "http://egg.snakelab.cc:8080/webhook/mono/'"$MY_MONO"'" }'