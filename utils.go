package main

import (
	"log"
	// "net"
	"strconv"
	"fmt"
	driver "github.com/neo4j/neo4j-go-driver/neo4j"
	"os"
	"strings"
  "github.com/shal/mono"
  "context"
  "github.com/gin-gonic/gin"
)

	

func opendb()(driver.Driver) {
	pass := strings.Split(os.Getenv("NEO4J_AUTH"),"/")[1]
	db, err := driver.NewDriver("bolt://neo4j:7687", driver.BasicAuth("neo4j", pass, ""))
	if err != nil {
		log.Println("error connecting to neo4j: ", err)
		// w.WriteHeader(500)
		// w.Write([]byte("An error occurred connecting to the DB"))
		return nil
	}
	return db
}
func createSession(db driver.Driver) (driver.Session) {
	session, err := db.Session(driver.AccessModeWrite)
  if err != nil {
      log.Println("error createSession: ", err)
      return nil
  }
  return session

}

func runQuery(session driver.Session, query string, params map[string]interface{}) (map[string]interface{}, error) {
  ret, err := session.WriteTransaction(func(transaction driver.Transaction) (interface{}, error) {
      result, err := transaction.Run(query, params)
      if err != nil {
          log.Println("runQuery error: ", err)
          return nil, err
      }

      if result.Next() {
          return result.Record().GetByIndex(0), nil
      }

      return nil, result.Err()
    })

  if err != nil {
    log.Println("error mergeStatement ", err)
    return nil, err
  }
  return ret.(driver.Node).Props(), err

}
func addToken(token string, telegramId int64, nickname string)(interface{}, error) {
	db := opendb()
	defer db.Close()
	session := createSession(db)
  defer session.Close()
  h := make(map[string]interface{})
  h["token"] = token
  h["telegramId"] = telegramId
  h["nickname"] = nickname
  _, err := runQuery(session, 
  	`MERGE (user:MONO_USER {id: $token})  
  	return user`, h)
  if err != nil {
    panic(err)
  }
  return runQuery(session, 
    `MATCH (mu: MONO_USER {id: $token})
    MERGE (u:TELEGRAM_USER {id: $telegramId})
    MERGE (u)<-[r:BELONGS {nickname: $nickname}]-(mu)
    return u`, h)
  
}

func getMonoUser(token string)(interface{}, error) {
	db := opendb()
	defer db.Close()
	session := createSession(db)
  defer session.Close()
  h := make(map[string]interface{})
  h["token"] = token
  
  return runQuery(session, 
  	`MERGE (user:MONO_USER {id: $token}) 
  	return user`, h)
}


func getTelegramUser(telegramId int64)(interface{}, error) {
  db := opendb()
  defer db.Close()
  session := createSession(db)
  defer session.Close()
  
  return runQuery(session, 
    `MATCH (user:TELEGRAM_USER {id: $telegramId}) 
    return user`, map[string]interface{} {"telegramId": telegramId})
}
func getTelegramUsersByTokenTx(tx driver.Transaction, token string)([]map[string]interface{}, error) {
  var list []map[string]interface{}

  result, err := tx.Run(`
    MATCH (tu: TELEGRAM_USER) <-[b:BELONGS]- (mu: MONO_USER {id: $token})
    RETURN tu.id as ID, b.nickname as NICKNAME
    `, map[string]interface{} {"token": token})
  if err != nil {
    return nil, err
  }
  for result.Next() {
    r := result.Record()
    value := make(map[string]interface{})

    if id, ok := r.Get("ID"); ok {
      value["id"] = id
    } 
    if nickname, ok := r.Get("NICKNAME"); ok {
      value["nickname"] = nickname
    }

    list = append(list, value)
  }
  return list, nil
}

func getTelegramUsersByToken(token string)([]map[string]interface{}, error) {
  db := opendb()
  defer db.Close()
  session := createSession(db)
  defer session.Close()
  tx, err := session.BeginTransaction()
  if err != nil {
    return nil, err
  }
  defer tx.Close()
  return getTelegramUsersByTokenTx(tx, token)
}

func getCurrencyStr(Id int)(string, error) {
  // db := opendb()
  // defer db.Close()
  // session := createSession(db)
  // defer session.Close()
  
  

  // result, err := session.Run(`
  //   MATCH (c: CURRENCY {id: $id})
  //   RETURN c.name as name
  //   `, map[string]interface{} {"id": Id})

  // if err != nil {
  //   return "", err
  // }
  
  // for result.Next() {
  //   r := result.Record()

  //   if v, ok := r.Get("name"); ok {
  //     if str, ok := v.(string); ok {
  //       return str, nil
  //     }
  //   } 
  // }
  if curr, err :=  mono.CurrencyFromISO4217(int32(Id)); err != nil {
    return "", err
  } else {
    return curr.Symbol, nil
  }
}

func getTokenList(telegramId int64)([]string, []string) {
	db := opendb()
	defer db.Close()
	session := createSession(db)
  defer session.Close()
  var nicknameList []string
  var tokenList []string

  _, err := session.WriteTransaction(func(transaction driver.Transaction) (interface{}, error) {
      result, err := transaction.Run("MATCH (tu:TELEGRAM_USER {id: $telegramId})<-[b:BELONGS]-(n:MONO_USER) RETURN b.nickname, n.id ORDER BY b.nickname LIMIT 10", map[string]interface{} {"telegramId": telegramId})
      if err != nil {
      		log.Println("runQuery error:", err)
          return nil, err
      }

      for result.Next() {
          nicknameList = append(nicknameList, result.Record().GetByIndex(0).(string))
          tokenList = append(tokenList, result.Record().GetByIndex(1).(string))
      }

      return nil, nil
    })

  if err != nil {
  	log.Println("error getTokenList ", err)
  }
  return nicknameList, tokenList
}


func delTokenTx(tx driver.Transaction, telegramId int64, token string)(string, error) {
  
  result, err := tx.Run("MATCH (tu:TELEGRAM_USER {id: $telegramId})<-[b:BELONGS]-(mu:MONO_USER {id: $id})  WITH b.nickname as nickname,b DELETE b RETURN nickname", map[string]interface{} {"telegramId":telegramId, "id": token})
  if err != nil {
      log.Println("delToken error: ", err)
      return "", err
  }

  if result.Next() {
      return result.Record().GetByIndex(0).(string), nil
  }

  return "", result.Err()
}

func delToken(telegramId int64, token string)(string, error) {
  db := opendb()
  defer db.Close()
  session := createSession(db)
  defer session.Close()
  tx, err := session.BeginTransaction()
  if err != nil {
    log.Println("delToken error2: ", err)
    return "", err
  }
  defer tx.Commit()
  return delTokenTx(tx, telegramId, token)
}

func delOrphanMonoUsersTx(tx driver.Transaction)(error) {
  log.Println("delete history of statements since nobody is following them")
  // _, err := transaction.Run("MATCH (s: STATEMENT)-[b:BELONGS]->(mu: MONO_USER) WHERE not (mu)-[:BELONGS]->() detach delete s, mu", nil)
  _, err := tx.Run(`
    MATCH (mu: MONO_USER) WHERE not (mu)-[:BELONGS]->()
    OPTIONAL MATCH (mu)<-[:BELONGS]-(s: STATEMENT)
    DETACH DELETE s, mu`, nil)
  if err != nil {
    log.Println("delOrphanMonoUsersTx error: ", err)
    return err
  }
  return nil
  
}

func delOrphanMonoUsers() {
  db := opendb()
  defer db.Close()
  session := createSession(db)
  defer session.Close()
  tx, err := session.BeginTransaction() 
  if err != nil {
    log.Println("delOrphanMonoUsers err", err)
    panic(err)
  }
  defer tx.Commit()

  if err := delOrphanMonoUsersTx(tx);err != nil {
    log.Println("delOrphanMonoUsers error2: ", err)
    panic(err) 
  }  
}

func registerMonoWebhook(token string)(interface{}, error) {
	personal := mono.NewPersonal(token)
	hookvalue := fmt.Sprintf("http://%s:8080/webhook/mono/%s", os.Getenv("MONO_HOSTNAME"), token)
  log.Println("hook value: ", hookvalue)
	return personal.SetWebHook(context.Background(), hookvalue)
	
}
func unregistrMonoWebhook(token string)(interface{}, error) {
  personal := mono.NewPersonal(token)
  return personal.SetWebHook(context.Background(), "")
}

func getMonoNickname(token string)(string, error) {
	personal := mono.NewPersonal(token)
	u, err := personal.User(context.Background()) 
  if err == nil {
    return u.Name, nil
  } else {
	  return "", err
  }
}


func countMonoUserFollowers(token string) (int64, error) {
  db := opendb()
  defer db.Close()
  session := createSession(db)
  defer session.Close()
  
  result, err := session.Run(`
    match (n: MONO_USER {id: $token}) -[:BELONGS]->(x:TELEGRAM_USER) return count(*)
    `, map[string]interface{} {"token": token})

  if err != nil {
    return 0, err
  }
  
  for result.Next() {
    r := result.Record()
    return r.GetByIndex(0).(int64), nil
     
  }
  return 0, nil
}

func addStatement(statement StatementItem)(interface{}, error) {
  db := opendb()
  defer db.Close()
  session := createSession(db)
  defer session.Close()
  ret, err := session.WriteTransaction(func(transaction driver.Transaction) (interface{}, error) {
        result, err := transaction.Run(
            `MERGE (s:STATEMENT {id: $id})
            MERGE (m:MCC {id: $mcc})
            MERGE (user:MONO_USER {id: $userId})
            MERGE (currency:CURRENCY {id: $currencyCode})
            MERGE (s)-[:BELONGS]->(m)
            MERGE (s)-[:BELONGS]->(user)
            MERGE (s)-[:BELONGS]->(currency)
            SET s.datetime = $time,
            s.desc = $description,
            s.hold = $hold,
            s.amount = $amount,
            s.operationAmount = $operationAmount,
            s.fee = $commissionRate,
            s.cashbackAmount = $cashbackAmount,
            s.balance = $balance,
            s.comment = $comment
            
            RETURN s`,
            statement.toMap())
        if err != nil {
        		log.Println("run error:", err)
            return nil, err
        }

        if result.Next() {
            return result.Record().GetByIndex(0), nil
        }
        return nil, result.Err()
    })
  return ret, err  
}

func IsProduction()(bool) {
  return gin.Mode() == gin.ReleaseMode
}


func getTokenBalance(token string)(string, error) {
 
  var balance int
  personal := mono.NewPersonal(token)
  user, err := personal.User(context.Background())
  if err != nil {
    return "", err
  }

  for _, acc := range user.Accounts {
    if acc.CurrencyCode == 980 {
      balance += acc.Balance
    }  
  }

  ccy, err := mono.CurrencyFromISO4217(980)
  if err != nil {
    return "", err
  } else {
    return fmt.Sprintf("%s %d.%d", ccy.Symbol, balance/100, balance%100), nil
  }
}
func getAdminId()(int64) {
  txt_id := os.Getenv("ADMIN_TELEGRAM_ID")
  id, err := strconv.Atoi(txt_id); 
  if err != nil {
    return -1
  }
  return int64(id)
}

func addError(msg string, stack string) {
  db := opendb()
  defer db.Close()
  session := createSession(db)
  defer session.Close()
  tx, err := session.BeginTransaction()
  if err != nil {
    panic(err)
  }
  defer tx.Commit()
  if err := addErrorTx(tx, msg, stack); err != nil {
    panic(err)
  }
}

func addErrorTx(tx driver.Transaction, msg string, stack string)(error) {
  _, err := tx.Run(`MERGE (e:ERROR {timestamp: timestamp()})
    SET e.msg = $msg, e.stack = $stack
    return e`, map[string]interface{} {"msg": msg, "stack": stack})
  return err
}


func getCount(cypher string)(int64,error) {
  db := opendb()
  defer db.Close()
  session := createSession(db)
  defer session.Close()
  tx, err := session.BeginTransaction()
  if err != nil {
    return 0, err
  }
  defer tx.Commit()
  if c, err := getCountTx(tx, cypher); err != nil {
    return 0, err
  } else{
    return c, nil}
}

func getCountTx(tx driver.Transaction, cypher string)(int64,error) {
  result, err := tx.Run(cypher, nil)

  if err != nil {
    return 0, err
  }
  
  for result.Next() {
    r := result.Record()
    return r.GetByIndex(0).(int64), nil
     
  }
  return 0, nil
}
