package main

func adminGetUsersCount()(int64, error) {
	return getCount("match (n:TELEGRAM_USER)<-[b:BELONGS]-(m:MONO_USER) return count(n)")
}

func adminGetStatementCount()(int64, error) {
	return getCount("match (s:STATEMENT) return count(s)")
}

func adminGetStatementCount24()(int64, error) {
	return getCount("match (s:STATEMENT) where s.datetime >= timestamp()/1000-60*60*24 return count(s)")
}

func adminGetErrorCount()(int64, error) {
	return getCount("match (e:ERROR) return count(e)")
}
func adminGetErrorCount24()(int64, error) {
	return getCount("match (e:ERROR) where e.timestamp/1000 >= timestamp()/1000-60*60*24 return count(e)")
}