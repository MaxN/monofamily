FROM ubuntu:18.04
WORKDIR /tmp
RUN apt update
RUN apt install -y libssl1.0.0 pkg-config wget git
RUN wget https://github.com/neo4j-drivers/seabolt/releases/download/v1.7.4/seabolt-1.7.4-Linux-ubuntu-18.04.tar.gz -O seabolt.tar.gz
RUN tar zxvf seabolt.tar.gz --strip-components=1 -C /
RUN wget https://dl.google.com/go/go1.13.4.linux-amd64.tar.gz -O golang.tar.gz
RUN tar -C /usr/local -xzf golang.tar.gz
ENV PATH=${PATH}:/usr/local/go/bin
ENV LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:/usr/local/lib
RUN go get github.com/neo4j/neo4j-go-driver/neo4j
RUN go get github.com/gin-gonic/gin
RUN go get github.com/go-telegram-bot-api/telegram-bot-api
RUN go get golang.org/x/sync/errgroup
RUN go get golang.org/x/crypto/acme/autocert
RUN go get google.golang.org/genproto/googleapis/cloud/dialogflow/v2
RUN go get -u github.com/go-delve/delve/cmd/dlv
RUN go get github.com/shal/mono


