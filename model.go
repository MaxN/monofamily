package main

import (
	"log"
	"fmt"
	"strings"
	
	"strconv"
)
const (
	NoCmd = 0
	AddCmd = 1
	ListCmd = 2
	DelCmd = 3
)

type Model struct {
	commandState map[int64]int
}
func NewModel() *Model {
	return &Model{make(map[int64]int)}
}

func (m *Model) AddStatement(statement StatementItem) {
	_, err := addStatement(statement)
	if err != nil {
		log.Println("AddStatement", err)
		return
	}
	currencyStr, err := getCurrencyStr(statement.CurrencyCode)
	if err != nil {
		log.Println("getCurrencyStr ", err)
		currencyStr = ""
	}
	telegram_user_list, err := getTelegramUsersByToken(statement.UserId)
	if err != nil {
		log.Println("AddStatement getTelegramUsersByToken", err)
		return 
	}
	log.Printf("telegram_user_list: %+v\n", telegram_user_list)
	
	for _, v := range telegram_user_list {
		
		_, err = notifyUser(v["id"].(int64), v["nickname"].(string), currencyStr, statement)
		if err != nil {
			log.Println("AddStatement notifyUser", err)
		}	
	}
	
}

func (m *Model) ShowTokensList(telegram_id int64)(string, error) {
	list, tokenList := getTokenList(telegram_id)
	var sb strings.Builder
  for i, v := range list {
  	balance, err := getTokenBalance(tokenList[i])
  	if err != nil {
  		sb.WriteString(fmt.Sprintf("%d. %s\n", i+1, v))
  	} else {
  		sb.WriteString(fmt.Sprintf("%d. %s (%s)\n", i+1, v, balance))

  	}
  }

  msg := sb.String()
  if msg == "" {
  	msg = "Список пуст"
  }
  return msg, nil

}
func (m *Model) DelToken(telegram_id int64, number int)(string, error) {

	_, tokenList := getTokenList(telegram_id)
	number = number - 1
	if number >= len(tokenList) {
		log.Printf("%d exceed id list len %d", number, len(tokenList))
		return  "Номер не верен", nil
	}

	c, err := countMonoUserFollowers(tokenList[number])
	if err != nil {
		return "", err
	}
	
	if c == 1 {
		if IsProduction() {
			if _, err := unregistrMonoWebhook(tokenList[number]); err != nil {
				log.Println("DelToken unregistrMonoWebhook", err)
				return "", fmt.Errorf("Не могу удалить веб хук. Попробуйте еще раз позже..\n%s", err.Error())
			}
		} else {
			log.Println("Monobank webhook is not unregistered")
		}
	}

	log.Printf("num=%d list=%+v", number, tokenList)
	nickname, err := delToken(telegram_id, tokenList[number])
	if  err != nil {
		return "", fmt.Errorf("Не могу удалить токен из БД.\n%s", err.Error())
	}

	delOrphanMonoUsers()
	
	return fmt.Sprintf("Удалил токен для %s", nickname), nil
}
func (m *Model) AddToken(token string, telegram_id int64, nickname string)(string, error) {
	log.Printf("adding token %s, telegram_id %d, nickname %s", token, telegram_id, nickname)
	mono_nick, err := getMonoNickname(token)
	if nickname == "" && err == nil {
		nickname = mono_nick
	}

	if err != nil {
		return "", fmt.Errorf("Не могу получить имя владельца токена: %s", err.Error())
	}

	if IsProduction() {
		_, err = registerMonoWebhook(token)
		if err != nil {
			return "", fmt.Errorf("Не могу установить веб хук для %s. Попробуйте еще раз через минуту \xF0\x9F\x98\x9E", nickname)
		}
	} else {
		log.Println("Monobank webhook is not registered")
	}
	
	_, err = addToken(token, telegram_id, nickname)
	if err != nil {
		return "", fmt.Errorf("Не могу добавить токен в БД: %s", err.Error())
	}

	log.Printf("Token for %s successfully added", nickname)
	return fmt.Sprintf("Токен для %s добавил \xF0\x9F\x98\x84", nickname), nil
}

func (m *Model) handleTelegramCommands(id int64, txt string) {
	state, ok := m.commandState[id]
	if !ok {
		state = NoCmd
	}
	if txt == "/cancel" {
		m.commandState[id] = NoCmd
		if state != NoCmd {
			sendMessage(id, "Команда отменена")
		} else {
			sendMessage(id, "Нечего отменять")
		}
		return
	}

	if state == NoCmd && txt == "/add" {
		if _, err := sendMessage(id, "Введите токен"); err != nil {
			panic(err)
		}
		m.commandState[id] = AddCmd
		return
	}
	if state == AddCmd {
		//add token
		nickname, err := getMonoNickname(txt)
		if err != nil {
			if _, err = sendMessage(id, "Токен не верный. Для отмены /cancel"); err != nil {
				panic(err)
			}
			return

		}
		resp, err := m.AddToken(txt, id, nickname)
		if err != nil {
			panic(fmt.Errorf("%s. Для отмены /cancel", err.Error()))
		} else {
			if _, err = sendMessage(id, resp); err != nil {
				panic(err)
			}
		}
		m.commandState[id] = NoCmd
		return
	}

	if state == NoCmd && txt == "/list" {
		if resp, err := m.ShowTokensList(id); err != nil {
			panic(err)
		} else {
			if _, err := sendMessage(id, resp); err != nil {
				panic(err)
			}
		}
		return
	}

	if state == NoCmd && txt == "/del" {
		if _, err := sendMessage(id, "Введите номер из списка"); err != nil {
			panic(err)
		}
		m.commandState[id] = DelCmd
		return
	}
	if state == DelCmd {
		//del 1
		if num, err := strconv.Atoi(txt); err != nil {
			panic(err)
		} else {
			if resp, err := m.DelToken(id, num); err != nil {
				panic(fmt.Errorf("%s. Для отмены /cancel", err.Error()))
			} else {
				if _, err = sendMessage(id, resp); err != nil {
					panic(err)
				}
			}
		}
		m.commandState[id] = NoCmd
		return
	}
  if state == NoCmd && txt == "/start" {
  	sendMessage(id, "Бот для получения уведомлений о движении средств членов вашей семьи и родственников. Для начала работы вам нужно получить токен. Токен получается тут https://api.monobank.ua. После этого добавьте токен с помощью команды /add. Если есть вопросы пишите @elserpiente. Если хотите помочь https://send.monobank.ua/4FNw2vnzL1. Исходники https://gitlab.com/MaxN/monofamily")
  	return
  }
  if state == NoCmd && txt == "/donate" {
  	sendMessage(id, "Чтоб купить мне шоколадку перейдите по ссылке https://send.monobank.ua/4FNw2vnzL1")
  	return
  }

	if txt == "/stats" && id == getAdminId() {
		fmt.Println("Admin command!")
		tel_users_count, err := adminGetUsersCount()
		if err != nil {
			panic(err)
		}
		statement_count, err := adminGetStatementCount()
		if err != nil {
			panic(err)
		}
		statement_count24, err := adminGetStatementCount24()
		if err != nil {
			panic(err)
		}
		error_count, err := adminGetErrorCount()
		if err != nil {
			panic(err)
		}
		error_count24, err := adminGetErrorCount24()
		if err != nil {
			panic(err)
		}

		sendMessage(id, fmt.Sprintf("\xF0\x9F\x83\x8F юзеров: %d\n\xF0\x9F\x93\x9C выписки: %d\n\xF0\x9F\x93\x9C выписки за \xF0\x9F\x95\x9E24ч: %d\n\xF0\x9F\x92\xA9 ошибки: %d\n\xF0\x9F\x92\xA9 ошибки \xF0\x9F\x95\x9E24ч: %d", tel_users_count, statement_count, statement_count24, error_count, error_count24))
		return
	}
	if txt == "/panic" && id == getAdminId() {
		panic(fmt.Errorf("Test Error"))
	}
	sendMessage(id, "А?")

}