package main

type StatementItem struct {
	Id string `json:"id"`
  Time int `json:"time"`
  Description string `json:"description"`
  UserId string `json:"userId"`
  Mcc int `json:"mcc"`
  Hold bool `json:"hold"`
  Amount int `json:"amount"`
  OperationAmount int `json:"operationAmount"`
  CommissionRate int `json:"commissionRate"`
  CashbackAmount int `json:"cashbackAmount"`
  Balance int `json:"balance"`
  CurrencyCode int `json:"currencyCode"`
  Comment string `json:"comment"`
}
type Data struct {
	Account string `json:"account"`
	StatementItemStruct StatementItem `json:"statementItem"`
}

type Statement struct {
	Type string `json:"type"`
	DataStruct Data `json:"data"`

}
func(s StatementItem) toMap() map[string]interface{} {
	ret := make(map[string]interface{})
	ret["id"] = s.Id
	ret["time"] = s.Time
	ret["description"] = s.Description
	ret["mcc"] = s.Mcc
	ret["hold"] = s.Hold
	ret["amount"] = s.Amount
	ret["operationAmount"] = s.OperationAmount
	ret["commissionRate"] = s.CommissionRate
	ret["cashbackAmount"] = s.CashbackAmount
	ret["balance"] = s.Balance
	ret["currencyCode"] = s.CurrencyCode
	ret["comment"] = s.Comment
	ret["userId"] = s.UserId
	return ret

}

