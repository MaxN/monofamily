package main
import (
	"log"
	// "net/http"
	"os"
	"fmt"
	telegram "github.com/go-telegram-bot-api/telegram-bot-api"
)


func notifyUser(telegram_id int64, nickname string, currency string, statement StatementItem)(interface{}, error) {
	// log.Printf("%+v", statement)
	var emoji_direction string
	if statement.Amount >=0 {
		emoji_direction = "\xF0\x9F\x91\x89\xF0\x9F\x92\xB3"
	} else {
		emoji_direction = "\xF0\x9F\x91\x88\xF0\x9F\x92\xB3"
	}

	msg := fmt.Sprintf("%s %s %s %.2f \xF0\x9F\x92\xAC %s\n\xF0\x9F\x92\xB0 %s %.2f баланс", emoji_direction, nickname, currency, float32(statement.Amount / 100.0) , statement.Description,
	currency,
	float32(statement.Balance / 100.0) )
	
	return sendMessage(telegram_id, msg)

}
func getBot()(*telegram.BotAPI, error) {
	bot, err := telegram.NewBotAPI(os.Getenv("MONO_TELE_BOT"))
	if err != nil {
		return nil, err
	}

	return bot, nil
}
func sendMessage(telegram_id int64, msg string)(interface{}, error) {
	log.Printf("sendMessage for telegram_id %d: %s\n", telegram_id, msg)
	m := telegram.NewMessage(telegram_id, msg)
	bot, err := getBot()
	if err != nil {
		return nil, err
	}
	return bot.Send(m)
}

func setTelegramWebHook()(interface{}, error) {
	bot, err := getBot()
	if err != nil {
		return nil, err
	}
	return bot.SetWebhook(telegram.NewWebhook(fmt.Sprintf("https://%s/webhook/tele/%s", os.Getenv("MONO_HOSTNAME"), os.Getenv("MONO_TELE_BOT"))))

}

func delTelegramWebHook()(interface{}, error) {
	bot, err := getBot()
	if err != nil {
		return nil, err
	}
	return bot.RemoveWebhook()

}
// func main() {
	
// 	bot, err := telegram.NewBotAPI("MyAwesomeBotToken")
// 	if err != nil {
// 		log.Fatal(err)
// 	}

// 	bot.Debug = true

// 	log.Printf("Authorized on account %s", bot.Self.UserName)

// 	_, err = bot.SetWebhook(telegram.NewWebhookWithCert("https://www.google.com:8443/"+bot.Token, "cert.pem"))
// 	if err != nil {
// 		log.Fatal(err)
// 	}
// 	info, err := bot.GetWebhookInfo()
// 	if err != nil {
// 		log.Fatal(err)
// 	}
// 	if info.LastErrorDate != 0 {
// 		log.Printf("Telegram callback failed: %s", info.LastErrorMessage)
// 	}
// 	updates := bot.ListenForWebhook("/" + bot.Token)
// 	go http.ListenAndServeTLS("0.0.0.0:8443", "cert.pem", "key.pem", nil)

// 	for update := range updates {
// 		log.Printf("%+v\n", update)
// 	}
// }