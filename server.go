package main

import (
	"log"
	"net"
	"net/http"
	"github.com/gin-gonic/gin"
	"io/ioutil"
	"bytes"
	
	"time"
	"golang.org/x/sync/errgroup"
	"golang.org/x/crypto/acme/autocert"
	"os"
	
	"encoding/json"
	"runtime/debug"
	// "errors"
	
)


var (
	g errgroup.Group
	model *Model
)

func Logger() gin.HandlerFunc {
    return func(c *gin.Context) {
      var bodyBytes []byte
      if c.Request.Body != nil {
        bodyBytes, _ = ioutil.ReadAll(c.Request.Body)
      }
      c.Request.Body = ioutil.NopCloser(bytes.NewBuffer(bodyBytes))
      
      log.Println(c.Request.Method, c.Request.URL, string(bodyBytes))

    }
}

func LogErrorRecovery() gin.HandlerFunc  {
	return func(c *gin.Context) {
		defer func() {
			if r := recover(); r != nil {
				addError(r.(error).Error(), string(debug.Stack()))
			}
		}()
	}
}

func TelegramRecovery() gin.HandlerFunc {
	    return func(c *gin.Context) {
	    	var telegram_id int64
	    	telegram_id = 0

	    	defer func() {
	    		if r := recover(); r != nil {
            log.Println("TelegramRecovery: ", r)
            log.Println("TelegramRecovery stacktrace: \n" + string(debug.Stack()))

            if telegram_id != 0 {
            	sendMessage(telegram_id, "\xF0\x9F\x92\xA9 Что-то пошло не так:\n"+r.(error).Error())	
            }
            addError(r.(error).Error(), string(debug.Stack()))
        	}

	    	}()
      	
      	var bodyBytes []byte
      	if c.Request.Body != nil {
        	bodyBytes, _ = ioutil.ReadAll(c.Request.Body)
      	}
      	c.Request.Body = ioutil.NopCloser(bytes.NewBuffer(bodyBytes))
      	
      	var j map[string]interface{}
      	if err := json.Unmarshal(bodyBytes, &j); err != nil {
					log.Println("TelegramRecovery JSON error: ", err)
					
				} else {
					
					msg, ok := j["message"].(map[string]interface{})
					if ok {
						chat, ok := msg["chat"].(map[string]interface{})
						if ok {
							id, ok := chat["id"].(float64)
							if ok {
								telegram_id = int64(id)
							}
						}
					}
						
				}
      	
      	c.Next()
    }
}

func monobank_webhook_router() http.Handler {
	r := gin.New()
	r.Use(Logger())
	r.Use(gin.Recovery())
	r.Use(LogErrorRecovery())

	r.LoadHTMLGlob("html/*")
	
	r.GET("/", func(ctx *gin.Context) {
		ctx.HTML(http.StatusOK, "index.html", gin.H{"test": 123})
	})	

	r.POST("/webhook/mono/:userid", func(c *gin.Context){

		var s Statement
		
		if err := c.ShouldBindJSON(&s); err != nil {
			log.Println("webhook json error: ", err)
			return
		}
		userid := c.Param("userid")
		if userid == "" {
			log.Println("userid is null")
			return
		}

		s.DataStruct.StatementItemStruct.UserId = userid
		model.AddStatement(s.DataStruct.StatementItemStruct)
		


	})
	return r

}


func telegram_webhook_router() http.Handler {
	r := gin.New()
	r.Use(Logger())
	r.Use(gin.Recovery())
	r.Use(TelegramRecovery())
	r.GET("/", func(ctx *gin.Context) {
		ctx.String(http.StatusOK, "ok")
	})	
	last_date := 0
	r.POST("/webhook/tele/" + os.Getenv("MONO_TELE_BOT"), func(c *gin.Context){
		defer c.String(http.StatusOK, "ok")
		var j map[string]interface{}

		if err := c.ShouldBindJSON(&j); err != nil {
			log.Println("webhook json error: ", err)
			return
		}
		msg := j["message"].(map[string]interface{})
		date := int(msg["date"].(float64))
		chat := msg["chat"].(map[string]interface{})
		txt := msg["text"].(string)
		id := int64(chat["id"].(float64))
		if date > last_date {
			last_date = date
			
			log.Println("new message for telegram id", id)
			model.handleTelegramCommands(id, txt)
			
		} 

	})
	return r
}



func main() {

	if _, err := setTelegramWebHook(); err != nil {
		log.Fatal("unable to set telegramwebhook")
	}
	log.Printf("IsProduction: %t", IsProduction())

	defer func(){
		log.Println("deleting telegram webhook")
		if _, err := delTelegramWebHook(); err != nil {
			log.Print("unable to delete telegramwebhook")
		}	
	}()

	model = NewModel()
	m := &autocert.Manager{
    Cache:      autocert.DirCache("secret-dir"),
    Prompt:     autocert.AcceptTOS,
    HostPolicy: autocert.HostWhitelist(os.Getenv("MONO_HOSTNAME")),
	}

	server01 := &http.Server{
		Handler:      monobank_webhook_router(),
		ReadTimeout:  5 * time.Second,
		WriteTimeout: 10 * time.Second,
		TLSConfig: m.TLSConfig(),
	}
	server02  := &http.Server{
		Addr: ":443",
		Handler:      telegram_webhook_router(),
		ReadTimeout:  5 * time.Second,
		WriteTimeout: 10 * time.Second,
		TLSConfig: m.TLSConfig(),
	}

	autcert_server := &http.Server {
		Handler: m.HTTPHandler(nil),
		Addr: ":80",
	}
	
	g.Go(func() error {

   
		l, err := net.Listen("tcp4", "0.0.0.0:8080")
		if err != nil {
	    log.Fatal(err)
		}
		// err = server01.ServeTLS(l, "cert.pem", "key.pem")
		err = server01.Serve(l)
		if err != nil && err != http.ErrServerClosed {
			log.Fatal(err)
		}
		return err
	})

	g.Go(func() error {
		err := server02.ListenAndServeTLS("", "")
		return err
	})

	g.Go(func() error {
		err := autcert_server.ListenAndServe()
    if err != nil {
        log.Fatalf("autcert_server.ListenAndServe() failed with %s", err)
    }
    return err

	})
	
	if IsProduction() {
    f, err := os.OpenFile("production.log", os.O_APPEND | os.O_CREATE | os.O_RDWR, 0666)
    if err != nil {
        log.Panicf("error opening file: %v", err)
    }
    defer f.Close()
    log.SetOutput(f)

	}

	if err := g.Wait(); err != nil {
		log.Fatal(err)
	}
	
}